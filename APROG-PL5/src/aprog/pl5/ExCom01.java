package aprog.pl5;

import java.util.Scanner;

public class ExCom01 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        int number;
        int result = 1;
        
        System.out.print("Escolha um número: ");
        number = in.nextInt();
        
        while(number/10 != 0){
            while(number!=0){
                result = result * (number % 10);
                number = number/10;
            }
            number = result;
            System.out.println(result);
            result = 1;
        }
        
    }
}
