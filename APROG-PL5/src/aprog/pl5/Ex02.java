/*
 * PL5 EX 2
 */
package aprog.pl5;

import javax.swing.JOptionPane;

public class Ex02 {

    public static void main(String[] args) {
        int n;
        int temp;
        n = Integer.parseInt(JOptionPane.showInputDialog("Quantos dias deseja introduzir a temperatura?"));
        for (int i = 1; i < n + 1; i++) {
            temp = Integer.parseInt(JOptionPane.showInputDialog("Temperatura no dia " + i + " :"));
            if (temp >= -30 && temp < 9) {
                System.out.println("Muito Frio");
            } else if (temp >= 9 && temp < 15) {
                System.out.println("Frio");
            } else if (temp >= 15 && temp < 20) {
                System.out.println("Ameno");
            } else if (temp >= 20 && temp < 30) {
                System.out.println("Quente");
            } else if (temp >= 30 && temp < 50) {
                System.out.println("Muito Quente");
            } else {
                System.out.println("Temperatura Extrema");
            }
        }
    }
}
