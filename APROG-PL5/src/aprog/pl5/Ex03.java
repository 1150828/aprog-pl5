/*
 * PL5 EX 3
 */
package aprog.pl5;

import javax.swing.JOptionPane;

public class Ex03 {

    public static void main(String[] args) {
        int idade;
        int i = 0;
        int maioridade = 0;
        String nome;
        do {
            nome = JOptionPane.showInputDialog("Introduza um nome (zzz para terminar): ");
            if (!nome.equalsIgnoreCase("zzz")) {
                do {
                    idade = Integer.parseInt(JOptionPane.showInputDialog("Introduza a idade de " + nome + ": "));
                } while (idade < 0);
                i++;

                if (idade >= 18) {
                    maioridade++;
                    System.out.println("Nome: " + nome + " | Idade: " + idade + " anos");
                }
            }
        } while (!nome.equalsIgnoreCase("zzz"));
        if (i == 0) {
            System.out.print("Nenhuma das pessoas inseridas têm mais de 18 anos.");
        } else {
            System.out.print("Percentagem de pessoas maiores de 18 anos é " + (float) (maioridade * 100 / i) + "%");
        }
    }
}
