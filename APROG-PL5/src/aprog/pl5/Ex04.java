/*
 * PL5 EX 4
 */
package aprog.pl5;

import java.util.Scanner;

public class Ex04 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num;
        int soma;
        int n;
        num=0;
        System.out.print("Escreva quantas repetições deseja fazer: ");
        n = in.nextInt();
        for (int i = n; i > 0; i--) {
            num = num+1;
            soma = 0;
            for (int j = num/2 + 1; j > 0; j--) {
                if (num % j == 0) {
                    soma = soma + j;
                }
            }
            if (num == soma) {
                System.out.println(num + " é igual a " + soma + " logo " + num + " é um número perfeito.");
            } else {
                //System.out.println(num + " é diferente de " + soma + " logo " + num + " não é um número perfeito.");
            }
            soma = 0;
        }
    }
}
