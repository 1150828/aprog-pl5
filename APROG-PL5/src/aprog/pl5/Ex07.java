/*
 * PL5 EX 7
 */
package aprog.pl5;

import java.util.Scanner;

public class Ex07 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a, b, temp, num, d, e;

        System.out.print("Introduza o valor a: ");
        a = in.nextInt();

        System.out.print("Introduza o valor b: ");
        b = in.nextInt();

        if (a > b) {
            temp = a;
            a = b;
            b = temp;
        }

        e = 0;

        System.out.print("Introduza o valor d: ");
        d = in.nextInt();

        for (int c = 0; c < d; c++) {
            do {
                System.out.print("Introduza um número: ");
                num = in.nextInt();
            } while (num < 0);
            if (num % a == 0 && b % num == 0) {
                e++;
            }
        }
        System.out.println("O resultado é " + e);

    }
}
