/*
 * PL5 EX 10
 */
package aprog.pl5;

import java.util.Scanner;

public class Ex10 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int number, ordem, resto, numCopy;
        double binary;

        System.out.print("Introduza um número: ");
        number = in.nextInt();
        numCopy = number;
        ordem = 0;
        binary = 0;

        while (number > 0) {
            resto = number % 2;
            binary = binary + resto * Math.pow(10, ordem);
            ordem++;
            number = number / 2;
        }
        System.out.println("O número decimal " + numCopy + " em binário é " + binary);
    }
}
