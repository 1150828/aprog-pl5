/*
 * PL5 EX 9
 */
package aprog.pl5;

import java.util.Scanner;

public class Ex09 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int number, numCopy, ordem, resto;
        double decimal;
        boolean binary = true;

        System.out.print("Introduza um número: ");
        number = in.nextInt();
        numCopy = number;
        ordem = 0;
        decimal = 0;

        while (number > 0) {
            resto = number % 10;
            if (!(resto == 0 || resto == 1)) {
                binary = false;
            }
            decimal = decimal + (resto * Math.pow(2, ordem));
            ordem++;
            number = number / 10;
        }

        if (binary) {
            System.out.println("O binário " + numCopy + " em decimal é " + (int) decimal);
        } else {
            System.out.println("O número introduzido não é binário");
        }
    }
}
