/*
 * PL5 EX 8
 */
package aprog.pl5;

import java.util.Scanner;

public class Ex08 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int number;
        boolean binary = true;

        System.out.print("Introduza um número: ");
        number = in.nextInt();

        while (number > 0) {
            if (!(number % 10 == 0 || number % 10 == 1)) {
                binary = false;
            }
            number = number / 10;
        }

        if (binary) {
            System.out.println("O número introduzido é binário");
        } else {
            System.out.println("O número introduzido não é binário");
        }
    }
}
