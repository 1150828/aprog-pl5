/*
 * PL5 EX 6
 */
package aprog.pl5;

import java.util.Scanner;

public class Ex06 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num, seqPares1, seqPares2, constante, constFinal;
        constFinal = 0;
        constante = 2;
        seqPares1 = 0;
        seqPares2 = 0;
        for (int i = 1; i < constante + 1; i++) {
            if (i == 2) {
                constFinal = -1;
            }
            do {
                System.out.print("Introduza um número na sequência " + i + " (" + constFinal + " para terminar): ");
                num = in.nextInt();
                if (num % 2 == 0 && num != constFinal) {
                    if (i == 1) {
                        seqPares1++;
                    } else if (i == 2) {
                        seqPares2++;
                    }
                }
            } while (num != constFinal);
        }
        System.out.print("Número de pares na primeira sequência são " + seqPares1 + " e na segunda sequência são " + seqPares2 + " ");
        if (seqPares1 > seqPares2) {
            System.out.print("logo a sequência com mais números pares é a primeira.\n");
        } else if (seqPares2 > seqPares2) {
            System.out.print("logo a sequência com mais números pares é a nenhuma.\n");
        } else {
            System.out.print("logo são as duas iguais.\n");
        }
    }
}
