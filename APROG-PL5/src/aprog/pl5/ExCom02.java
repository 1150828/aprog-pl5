/*
 * PL5 EX COMPLEMENTAR 2
 */
package aprog.pl5;

import java.util.Scanner;

public class ExCom02 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        int val1 = 0;
        int val2 = 1;
        int soma = 0;
        int n;
        
        System.out.print("Introduza quantos termos da sucessão de Fibonacci deseja: ");
        n=in.nextInt();
        
        for(int i=n; i>0; i--){
            System.out.println(soma);
            soma = val1 + val2;
            val1 = val2;
            val2 = soma;
        }
    }
}
