/*
 * PL5 EX 1
 */
package aprog.pl5;

import javax.swing.JOptionPane;

public class Ex01 {

    public static void main(String[] args) {
        int n;
        int num;
        int somaPares;
        int pares;
        double mediaPares;
        n = Integer.parseInt(JOptionPane.showInputDialog("Quantas vezes deseja correr?"));
        pares = 0;
        somaPares = 0;
        for (int i = 1; i < n + 1; i++) {
            num = Integer.parseInt(JOptionPane.showInputDialog("Número " + i + " :"));
            if (num % 2 == 0) {
                pares++;
                somaPares = somaPares + num;
            }
        }
        if (pares != 0) {
            mediaPares = somaPares / pares;
            System.out.println("A média dos pares introduzidos é " + mediaPares + " e a percentagem de pares introduzidos é " + (double) pares / n);
        } else {
            System.out.println("Não foi introduzido nenhum par.");
        }
    }
}
