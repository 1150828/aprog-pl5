/*
 * PL5 EX 5
 * a) O algoritmo tem como função ler qualquer número
 * e reescrever esse número com apenas os algarismos
 * impares desse número.
 */
package aprog.pl5;

import java.util.Scanner;

public class Ex05 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num, d, aux, res;
        res = 0;
        aux = 1;
        System.out.print("Introduza um número: ");
        num = in.nextInt();
        while (num != 0) {
            d = num % 10;
            if (d % 2 == 1) {
                res = res + d * aux;
                aux = aux * 10;
            }
            num = num / 10;
        }
        System.out.println("O resultado apenas com impares é " + res);
    }
}
